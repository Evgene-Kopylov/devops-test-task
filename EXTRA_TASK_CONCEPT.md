# Задание №3 (Middle)
Предложить инструмент и настроить запуск этого узла на кластере из
нескольких серверов. Кол-во машин в кластере может быть от 10 до
нескольких тысяч. Предоставить инструкцию и алгоритм запуска.
(оркестраторы, напр. Kubernetes)

# Решение.
Для развертки масштабируемой системы следует использовать докер-контейнер
единичного узла.
В папке `/morph/` Dockerfile для узла Морф.

Готовый контейнер доступен командой
```commandline
docker pull eugene8571/morph-node:latest
docker run eugene8571/morph-node:latest
```

Два контейнера, работающих одновременно и не мешают друг другу.
![img_3.png](img_3.png)

### 1. Подготовка управляющего узла (Master Node).
Управляющий скрипт на отдельной машине.
- Python 
- возможно дополнить сборщиком данных 
- ТГ бот в качестве интерфейса. (Уведомления, состояние, типовые команды)
![img_4.png](img_4.png)

### 2. Получение сервера.
Если управляющий узел, в результате команды, или по алгоритмам,
решит добавить сервер, он обращается к провайдеру по API, сообщает какой сервер
ему нужен и какой снапшот на него загрузить.
![img_5.png](img_5.png)

То же и для остановки лишней машины
![img_6.png](img_6.png)

### 3. Запуск контейнеров на новой машине.
Использовать Кубернетис или другой популярный инструмент. Инструкцию по развертке я бы взял у [Провайдера](https://help.reg.ru/support/servery-vps/oblachnyye-servery/ustanovka-programmnogo-obespecheniya/rukovodstvo-po-kubernetes#5).

![img_7.png](img_7.png)


kubeadm join 128.140.116.239:6443 --token xthruh.msyqggs90s76lk4w --discovery-token-ca-cert-hash sha256:58987a6362664f698bc2fd246785bf01d2d5cc030213a2b10ba53304e01cfe2c
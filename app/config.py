from typing import Optional

from pydantic_settings import BaseSettings
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class Settings(BaseSettings):
    TELEGRAM_BOT_TOKEN: str = None  # Телеграмм токен
    COINMARKETCAP_API_KEY: str = None
    TG_GROUP: str = None

    # URL для доступа к CoinMarketCap API
    COINMARKETCAP_URL: str = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'

    CHECKS_INTERVAL: int = 300  # интервал периодических проверок


settings = Settings()

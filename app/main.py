from pprint import pprint

import telebot
import requests
import time
from threading import Thread
from telebot.types import Message

from config import settings

# Инициализация бота
bot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN)

# Хранилище пороговых значений
thresholds = {}


# Команда /start
@bot.message_handler(commands=['start'])
def send_welcome(message: Message):
    bot.reply_to(message, 'Привет! Используйте /set <валюта> <min> <max> для установки порогов.'
                          '\nк примеру, "/set ETH 3471.00 3472.0"')


# Команда /set для установки порогов
@bot.message_handler(commands=['set'])
def set_threshold(message: Message):
    try:
        args = message.text.split()[1:]
        if len(args) != 3:
            bot.reply_to(message, 'Используйте формат: /set <валюта> <min> <max>')
            return

        currency, min_value, max_value = args[0].upper(), float(args[1]), float(args[2])
        thresholds[currency] = {'min': min_value, 'max': max_value}
        bot.reply_to(message, f'Установлены пороги для {currency}: min = {min_value}, max = {max_value}')
        # Запуск функции проверки цен в отдельном потоке
        Thread(target=check_prices).start()

    except ValueError:
        bot.reply_to(message, 'Неправильный формат чисел.')


# Функция для проверки цен
def check_prices():
    interval = settings.CHECKS_INTERVAL
    limit = 2 * 24 * 60 * 60  # 2 days

    # Время жизни отдельного потока конечно.
    for _ in range(int(limit/interval)):
        print("Проверка...")
        for currency, limits in thresholds.items():
            params = {
                'symbol': currency,
                'convert': 'USD'
            }
            headers = {
                'X-CMC_PRO_API_KEY': settings.COINMARKETCAP_API_KEY
            }
            response = requests.get(settings.COINMARKETCAP_URL, headers=headers, params=params)
            data = response.json()

            if 'data' in data and currency in data['data']:
                price = data['data'][currency]['quote']['USD']['price']
                print(f"{price=}")
                if price < limits['min'] or price > limits['max']:
                    bot.send_message(settings.TG_GROUP, f'Цена {currency} достигла {price} USD')

        time.sleep(interval)  # Проверка каждые n секунд


# Запуск бота
if __name__ == "__main__":
    bot.polling()

Для установки и настройки системы мониторинга на сервере Ubuntu 20 | 22

# Установка Prometheus
Создание пользователя для Prometheus:

```commandline
sudo useradd --no-create-home --shell /bin/false prometheus
```

Создание директорий:

```commandline
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus
```

Загрузка и установка Prometheus:

```commandline
cd /tmp
curl -LO https://github.com/prometheus/prometheus/releases/download/v2.29.1/prometheus-2.29.1.linux-amd64.tar.gz
tar xvf prometheus-2.29.1.linux-amd64.tar.gz
cd prometheus-2.29.1.linux-amd64
sudo cp prometheus /usr/local/bin/
sudo cp promtool /usr/local/bin/
sudo cp -r consoles /etc/prometheus
sudo cp -r console_libraries /etc/prometheus
sudo cp prometheus.yml /etc/prometheus
sudo chown -R prometheus:prometheus /etc/prometheus /var/lib/prometheus
```

Создание службы Prometheus:


```commandline
sudo nano /etc/systemd/system/prometheus.service
```
Вставьте следующее содержание в файл:
```text
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus --config.file /etc/prometheus/prometheus.yml --storage.tsdb.path /var/lib/prometheus --web.console.templates=/etc/prometheus/consoles --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
```
Запуск Prometheus:

```commandline
sudo systemctl daemon-reload
sudo systemctl start prometheus
sudo systemctl enable prometheus
```

Проверка работы Prometheus:

Откройте браузер и перейдите по адресу http://<your_server_ip>:9090.
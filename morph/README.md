`Dockerfile` - сборка контейнера Morph узла.

Готовый контейнер можно взять с `docker.io`
```commandline
docker pull eugene8571/morph-node:latest
```

запуск
```commandline
docker run eugene8571/morph-node:latest
```

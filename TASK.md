# Задание №1 (Обязательное)
 
- На выданном удаленном сервере с Ubuntu 20.04 настроить доступ по
SSH ключу, отключить доступ для всех пользователей по паролю.
- Установить и настроить систему мониторинга (жизненно-важные
показатели сервера в виде дашборда) для отслеживания потребления
ресурсов и состояния (любая на выбор исполнителя) с просмотром
состояния через web страницу.
 
Условия сдачи:
- предоставить данные по SSH ключу для входа на сервер;
Предоставить пароль от root;
- дать ссылку на web-мониторинг состояния ресурсов, ip:port или по
домену (на выбор исполнителя), данные для входа если есть
авторизация;
  
# Задание №2 (Обязательное)
 
На том же сервере запустить скрипт для отслеживания курса
выбранной криптовалюты через API сайта https://coinmarketcap.com/.
Курс отслеживается к паре USD.
Пользователь задает интересующую валюту и пороговое значение
через телеграм, каким способом на усмотрение исполнителя (пишет в
чате/канале или через бота или др.).
Уведомления о достижении порогового курса должны поступать в
чат/канал телеграм.
 
Условия сдачи:
- реализация на Python (или js или ts);
- возможность отслеживания нескольких криптовалют, хранение
минимум двух значений для каждой валюты (условно максимальный и
минимальный порог);
- предоставить краткий гайд по запуску скрипта на сервере и заданию
курсов в телеграм;
- дать доступ по ссылке к телеграм чату/каналу куда бот шлет
уведомления о состоянии сервера.

# Задание №3 (Не обязательное, но желательное, т.к. покажет ваш уровень знаний devops)
 
- Запустить блокчейн узел Morph.
https://docs.morphl2.io/docs/build-on-morph/developer-resources/how-to-run-a-morph-node
 
Условия сдачи:
- Скриншот с логами на примере этого:
https://disk.yandex.ru/i/0aZqb7-TN6M4dg
Запущенный сервис отобразится как active :active (running)
Работающий узел будет набирать высоту постепенно. Если высота не
набирается (не увеличивается) – где-то допущена ошибка. Высота
может начинать набираться не сразу, изначально можете получить
статус отсутствия пиров. Это нормально, но если через пару часов
ничего не меняется, возможна ошибка в ходе выполнения.
_____________________________________________
Срок на выполнение - 48 часов с момента отправки задания. Через 24
часа прошу написать, выполняете вы еще задание или отказались
(освободим сервер для других кандидатов). Если не написали – считаю,
что решили не выполнять.
Реальное время, которое затратит разработчик-devops на выполнение -
1 час.
Результат выполнения задания предоставить в текстовом файле в
облаке (как этот)
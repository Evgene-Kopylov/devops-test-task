Задание №1 (Обязательное)
 
- [x] На выданном удаленном сервере с Ubuntu 20.04 настроить доступ по
SSH ключу, отключить доступ для всех пользователей по паролю.

подключение
```bash
ssh root@128.140.105.2
ssh root@128.140.116.239
ssh root@5.161.65.77
```

новый пароль
```commandline
NewPassword123
```
добавить ssh pub ключ на сервер
```commandline
ssh-copy-id -i /home/death/id_rsa_dtt.pub root@128.140.105.2
ssh-copy-id -i /home/death/id_rsa_dtt.pub root@128.140.116.239
ssh-copy-id -i /home/death/id_rsa_dtt.pub root@5.161.65.77
```
зайти на сервер по ключу
```commandline
ssh -i /home/death/id_rsa_dtt root@128.140.105.2
ssh -i /home/death/id_rsa_dtt root@128.140.116.239
ssh -i /home/death/id_rsa_dtt root@5.161.65.77
```
sudo systemctl restart ssh
```commandline
sudo service ssh restart
```
проверка
```commandline
ssh root@128.140.105.2 -o PreferredAuthentications=password
```
нашел и отредактировал дополнительный файл настроек sudo nano `/etc/ssh/sshd_config.d/50-cloud-init.conf`

```commandline
$ ssh root@128.140.105.2 -o PreferredAuthentications=password
root@128.140.105.2: Permission denied (publickey).

```

- [ ] Установить и настроить систему мониторинга (жизненно-важные
показатели сервера в виде дашборда) для отслеживания потребления
ресурсов и состояния (любая на выбор исполнителя) с просмотром
состояния через web страницу.
 
Условия сдачи:
- предоставить данные по SSH ключу для входа на сервер;
Предоставить пароль от root;
- дать ссылку на web-мониторинг состояния ресурсов, ip:port или по
домену (на выбор исполнителя), данные для входа если есть
авторизация;
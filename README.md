# Тестовое задание Python + DevOps

### Выполнил [@EvgeneKopylov](https://t.me/EvgeneKopylov)

### [TASK.md](TASK.md)

# Задание №1

## SSH ключи

[id_rsa_dtt](id_rsa_dtt) - приватный

[id_rsa_dtt.pub](id_rsa_dtt.pub) - публичный


## root pwd
```commandline
Fx9iCuisdf213
```

## Prometheus
http://128.140.105.2:9090/

## Grafana
http://128.140.105.2:3000/


Login
```commandline
admin
```
PWD
```commandline
ajkla786LJ
```

http://128.140.105.2:3000/d/UDdpyzz7z/prometheus-2-0-stats?orgId=1&refresh=1m

![img.png](img.png)


# Задание №2 

## Уведомления о состоянии сервера
Приходят в группу https://t.me/dtt_notifications


## Описание бота

Бот активен в группе https://t.me/dtt_notifications

Бот принимает запрос (в группе) на отслеживание курса монеты. 

Проверки, по умолчанию, происходят каждые 300 секунд.

Если цена выпадает из интервала, бот делает уведомление в группе.

Возможно отслеживать несколько монет.

## Запуск бота локально
- в среде разработки `python app/main.py`
- либо через [docker-compose.yml](docker-compose.yml)

* `.env` имеет значения по умолчанию, но возможно указать свежие.
Доступрые переменные можно увидеть в `app/config.py`

## Запуск на сервере
Через Gitlab CI, используя готовый контейнер [docker-compose.deploy.yml](docker-compose.deploy.yml)


## Использование 
`/start` - начало работы + подсказка

`/set <coin> <min> <max>` - задать монету и интервал цены в долларах.

Пример для Эфира.
```text
/set ETH 3471.00 3472.0
```
Можно задать несколько позиций для отслеживания.

# Задание №3

В папке `morph`, `Dockerfile` - сборка контейнера узла.

Готовый контейнер можно взять с `docker.io`
```commandline
docker pull eugene8571/morph-node:latest
```

запуск
```commandline
docker run eugene8571/morph-node:latest
```


![img_1.png](img_1.png)

![img_2.png](img_2.png)
